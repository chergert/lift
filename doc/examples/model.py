from lift import models

class Channel(models.Model):
    # The name of the channel: "SciFi"
    name = models.CharField()

    # The channel number/frequency: 420
    number = models.IntField()

    # A pixmap image for the channel
    pixbuf = models.BufferField(lazy = True)
