from lift import services

class VideoService(services.Service):
    name = "video"

    def start_service(self):
        self.tv_tuner = Tuner('/dev/tuner')

    def stop_service(self):
        self.tv_tuner.close()

    def get_video_for_channel(self, num):
        self.tv_tuner.set_channel(num)
        return ChannelSource(self.tv_tunner)
