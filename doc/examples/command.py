from lift import commands
from gettext import gettext as _

class RemoveChannel(commands.Command):
    text = _("remove channel")
    can_undo = True
    can_redo = True

    def __init__(self, channels, channel):
        self._channels = channels
        self._channel = channel

    def do(self):
        if self._channel not in self._channels:
            raise AttributeError, 'Channel not found'
        self._channels.remove(self._channel)

    def undo(self):
        self._channels.add(self._channel)

    def chain(self):
        # Never allow chaining, each command is individually undoable
        return False
