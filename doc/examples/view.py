from lift import views
from gettext import gettext as _

class TvView(views.View):
    views.layout('glade', filename = 'tvview.glade')

    def initialize(self):
        """This method is called after view object binding to allow for
        adjusting the ui as needed."""
        # main_vbox is auto bound from the gtkui builder file
        self.main_vbox.set_border_width(12)

        # We are a window, lets set a proper title
        self.window.set_title(_("Tv Viewer"))
