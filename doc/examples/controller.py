from lift import controllers, Runtime

class TvController(controllers.Controller):
    """
    TvController manages the particular view for watching tv.
    """
    # view is attached during the wiring process for the controller.
    # this allows for potentially multiple view implementations.
    controllers.view('tv')

    def change_channel(self, num):
        VideoService = Runtime.get_service('video')

        # get a live channel stream
        source = VideoService.get_video_for_channel(num)

        # update our video widget to play the source
        self.view.video.set_source(source)

        # update our current channel label
        self.view.channel_label.set_text('<b>%d</b>' % num)

    def power_on(self):
        self.view.video.show()
        self.view.video.unmute()

    def power_off(self):
        self.view.video.mute()
        self.view.video.hide()
