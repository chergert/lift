from lift import settings

class TvSettings(settings.Settings):
    x_pos = settings.IntSetting('x_pos')
    y_pos = settings.IntSetting('y_pos')
    width = settings.IntSetting('width')
    height = settings.IntSetting('height')

    # The meta class will provide methods to monitor these
    # settings for changes.  This can be used by your application
    # to update the ui for example.  Hook them up from the
    # controller to update the view.

    # def setup_controller(self):
    #     win_settings = Runtime.get_setting('main_window')
    #
    #     win_settings.width_setting.changed(lambda w: self.set_width(w))
    #     win_settings.height.changed(lambda h: self.set_height(h))
    #     
    # def set_width(self, width):
    #     height = self.view.allocation.height
    #     self.view.resize(width, height)

# This will register the settings at /GCONF_ROOT/main_window/
TvSettings = TvSettings("main_window")
