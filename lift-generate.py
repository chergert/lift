#!/usr/bin/env python

import getopt
import sys
import os

TEMPLATE_CONTROLLER = """
from lift.controllers import Controller, view

class %(class)sController(Controller):
    view('%(view)s')
    auto = %(auto)s
"""

TEMPLATE_VIEW = """
from lift.views import View, layout

class %(class)sView(View):
    layout('%(layout)s')
    name = "%(name)s"
"""

TEMPLATE_SETTING = """
from lift import settings

class %(class)sSettings(Settings):
    test = settings.IntSetting()
"""

def green(word):
    return '\033[01;32m%s\033[00m' % word

def red(word):
    return '\033[01;31m%s\033[00m' % word

def touch(filename):
    sys.stdout.write('\tCreating "%s" ' % filename)
    sys.stdout.write(' ' * (50 - len(filename)))
    sys.stdout.flush()
    try:
        file(filename, 'a').close()
        print green('  done')
    except:
        print red('failed')

def authors(filename):
     authorname = [line.split(':')[4].split(',')[0] for line in file('/etc/passwd') if line.startswith(os.environ['USER'])][0]
     file(filename, 'w').write(authorname + '\n')

def mkdir(dirname):
    sys.stdout.write('\tCreating "%s" ' % dirname)
    sys.stdout.write(' ' * (50 - len(dirname)))
    sys.stdout.flush()
    try:
        os.makedirs(dirname)
        print green('  done')
    except:
        print red('failed')

def startup(filename, name):
    f = file(filename, 'w')
    f.write('#!/usr/bin/env python\n')
    f.write('from lift import main\n\n')
    f.write('main("%s")\n' % name)
    f.close()
    os.system("chmod +x %s" % filename)

def setup_py(filename, name):
    # create our setup tools file
    template = """#!/usr/bin/env python
from setuptools import setup, find_packages

setup(
    name     = "%(name)s",
    version  = "0.1.0",
    packages = find_packages(),
    scripts  = ['%(name)s'],

    author       = "",
    author_email = "",
    description  = "",
    license      = "",
    keywords     = "",
    url          = "",
)
""" % {'name': name}
    sys.stdout.write('\tCreating "%s" ' % filename)
    sys.stdout.write(' ' * (50 - len(filename)))
    sys.stdout.flush()
    try:
        if os.path.exists(filename):
            raise IOError, 'File exists'
        f = file(filename, 'w')
        f.write(template)
        f.close()
        os.system("chmod +x " + filename)
        print green('  done')
    except Exception, ex:
        print red('failed')
        print '\t\t', str(ex)

def setup_git(dirname):
    print ''
    print 'Setting up git repository'
    print ''
    sys.stdout.write('\t')
    sys.stdout.flush()
    curdir = os.getcwd()
    os.chdir(dirname)
    try:
        os.system("git init")
    except Exception, ex:
        print '\tError setting up git: %s' % str(ex)
    os.chdir(curdir)
    print ''

def glade(filename):
    print ''
    msg = 'Generating glade layout'
    sys.stdout.write(msg + ' ' * (70 - len(msg)))
    sys.stdout.flush()
    try:
        if os.path.exists(filename):
            raise IOError, 'file exists'
        dirname = os.path.dirname(filename)
        os.makedirs(dirname)
        f = file(filename, 'w')
        f.write(TEMPLATE_GLADE)
        f.close()
        print green('  done')
    except Exception, ex:
        print red('failed')
        print '\t%s' % str(ex)

def controller(filename, name = 'MainController', view = "main"):
    sys.stdout.write('\tCreating "%s" ' % filename)
    sys.stdout.write(' ' * (50 - len(filename)))
    sys.stdout.flush()
    try:
        if os.path.exists(filename):
            raise IOError, 'File Exists'
        f = file(filename, 'w')
        f.write('from lift.controllers import Controller, view\n')
        f.write('\n')
        f.write('class %s(Controller):\n' % name)
        f.write('    view("%s")\n' % view)
        f.write('    auto = True\n')
        f.close()
        print green('  done')
    except Exception, ex:
        print red('failed')
        print '\t' + str(ex)

def view(filename, name = 'MainView', view = 'main', layout = 'glade'):
    sys.stdout.write('\tCreating "%s" ' % filename)
    sys.stdout.write(' ' * (50 - len(filename)))
    sys.stdout.flush()
    try:
        if os.path.exists(filename):
            raise IOError, 'File Exists'
        f = file(filename, 'w')
        f.write('from lift.views import View, layout\n')
        f.write('\n')
        f.write('class %s(View):\n' % name)
        f.write('    layout("%s")\n' % layout)
        f.write('    name = "%s"\n' % view)
        f.close()
        print green('  done')
    except Exception, ex:
        print red('failed')
        print '\t' + str(ex)

def project_tool(args):
    if not len(args):
        print 'usage: %s project [create] name' % sys.argv[0]
        sys.exit(1)

    command = args[0]

    if command == 'create':
        if not len(args[1:]):
            print 'no project name specified'
            return

        project = args[1].lower()

        print "Creating project", project
        print ''

        mkdir(os.path.join(project, project))
        touch(os.path.join(project, project, '__init__.py'))
        mkdir(os.path.join(project, project, 'controllers'))
        touch(os.path.join(project, project, 'controllers', '__init__.py'))
        controller(os.path.join(project, project, 'controllers', 'main.py'))
        mkdir(os.path.join(project, project, 'models'))
        touch(os.path.join(project, project, 'models', '__init__.py'))
        mkdir(os.path.join(project, project, 'settings'))
        touch(os.path.join(project, project, 'settings', '__init__.py'))
        mkdir(os.path.join(project, project, 'commands'))
        touch(os.path.join(project, project, 'commands', '__init__.py'))
        mkdir(os.path.join(project, project, 'services'))
        touch(os.path.join(project, project, 'services', '__init__.py'))
        mkdir(os.path.join(project, project, 'views'))
        touch(os.path.join(project, project, 'views', '__init__.py'))
        view(os.path.join(project, project, 'views', 'main.py'))
        touch(os.path.join(project, project, 'config.py'))
        mkdir(os.path.join(project, 'data', 'views'))
        touch(os.path.join(project, project, 'ChangeLog'))
        touch(os.path.join(project, project, 'README'))
        setup_py(os.path.join(project, 'setup.py'), project[0].upper() + project[1:])
        authors(os.path.join(project, 'AUTHORS'))
        startup(os.path.join(project,
                             project[0].upper() + project[1:]),
                project)
        glade(os.path.join(project, 'data', 'views', 'main', 'mainview.glade'))
        setup_git(project)

def controller_tool(*args):
    pass

def setting_tool(*args):
    pass

def view_tool(*args):
    pass

tools = {
    'project':    project_tool,
    'controller': controller_tool,
    'setting':    setting_tool,
    'view':       view_tool,
}

def usage(stream = sys.stdout):
    print 'usage: lift-generate.py TOOL [OPTIONS]'
    print ''
    print 'Options:'
    print ''
    print '  -h, --help    Show this help menu'
    print ''
    print 'Tools:'
    print ''
    print '  controller    Tool for working with controllers'
    print '  view          Tool for working with views'
    print '  setting       Tool for working with settings'
    print '  project       Tool for working with projects'
    print ''

def main(argv = sys.argv):
    try:
        opts, args = getopt.getopt(argv[1:], 'h', ['help'])
    except getopt.GetoptError:
        usage(sys.stderr)
        sys.exit(1)

    for o,a in opts:
        if o in ('-h', '--help'):
            usage()
            sys.exit()

    if not len(args):
        usage(sys.stderr)
        print 'No tool was specified'
        sys.exit(1)

    tool = args[0]

    if tool not in tools:
        print 'Tool "%s" is unknown' % tool
        sys.exit(1)

    tools[tool](args[1:])

TEMPLATE_GLADE = \
"""<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!DOCTYPE glade-interface SYSTEM "glade-2.0.dtd">
<!--Generated with glade3 3.4.5 on Tue Nov 25 01:26:03 2008 -->
<glade-interface>
  <widget class="GtkWindow" id="window1">
    <property name="visible">True</property>
    <child>
      <widget class="GtkVBox" id="vbox1">
        <property name="visible">True</property>
        <child>
          <widget class="GtkMenuBar" id="menubar1">
            <property name="visible">True</property>
            <child>
              <widget class="GtkMenuItem" id="menuitem1">
                <property name="visible">True</property>
                <property name="label" translatable="yes">_File</property>
                <property name="use_underline">True</property>
                <child>
                  <widget class="GtkMenu" id="menu1">
                    <property name="visible">True</property>
                    <child>
                      <widget class="GtkImageMenuItem" id="imagemenuitem1">
                        <property name="visible">True</property>
                        <property name="label" translatable="yes">gtk-new</property>
                        <property name="use_underline">True</property>
                        <property name="use_stock">True</property>
                      </widget>
                    </child>
                    <child>
                      <widget class="GtkImageMenuItem" id="imagemenuitem2">
                        <property name="visible">True</property>
                        <property name="label" translatable="yes">gtk-open</property>
                        <property name="use_underline">True</property>
                        <property name="use_stock">True</property>
                      </widget>
                    </child>
                    <child>
                      <widget class="GtkImageMenuItem" id="imagemenuitem3">
                        <property name="visible">True</property>
                        <property name="label" translatable="yes">gtk-save</property>
                        <property name="use_underline">True</property>
                        <property name="use_stock">True</property>
                      </widget>
                    </child>
                    <child>
                      <widget class="GtkImageMenuItem" id="imagemenuitem4">
                        <property name="visible">True</property>
                        <property name="label" translatable="yes">gtk-save-as</property>
                        <property name="use_underline">True</property>
                        <property name="use_stock">True</property>
                      </widget>
                    </child>
                    <child>
                      <widget class="GtkSeparatorMenuItem" id="separatormenuitem1">
                        <property name="visible">True</property>
                      </widget>
                    </child>
                    <child>
                      <widget class="GtkImageMenuItem" id="imagemenuitem5">
                        <property name="visible">True</property>
                        <property name="label" translatable="yes">gtk-quit</property>
                        <property name="use_underline">True</property>
                        <property name="use_stock">True</property>
                      </widget>
                    </child>
                  </widget>
                </child>
              </widget>
            </child>
            <child>
              <widget class="GtkMenuItem" id="menuitem2">
                <property name="visible">True</property>
                <property name="label" translatable="yes">_Edit</property>
                <property name="use_underline">True</property>
                <child>
                  <widget class="GtkMenu" id="menu2">
                    <property name="visible">True</property>
                    <child>
                      <widget class="GtkImageMenuItem" id="imagemenuitem6">
                        <property name="visible">True</property>
                        <property name="label" translatable="yes">gtk-cut</property>
                        <property name="use_underline">True</property>
                        <property name="use_stock">True</property>
                      </widget>
                    </child>
                    <child>
                      <widget class="GtkImageMenuItem" id="imagemenuitem7">
                        <property name="visible">True</property>
                        <property name="label" translatable="yes">gtk-copy</property>
                        <property name="use_underline">True</property>
                        <property name="use_stock">True</property>
                      </widget>
                    </child>
                    <child>
                      <widget class="GtkImageMenuItem" id="imagemenuitem8">
                        <property name="visible">True</property>
                        <property name="label" translatable="yes">gtk-paste</property>
                        <property name="use_underline">True</property>
                        <property name="use_stock">True</property>
                      </widget>
                    </child>
                    <child>
                      <widget class="GtkImageMenuItem" id="imagemenuitem9">
                        <property name="visible">True</property>
                        <property name="label" translatable="yes">gtk-delete</property>
                        <property name="use_underline">True</property>
                        <property name="use_stock">True</property>
                      </widget>
                    </child>
                  </widget>
                </child>
              </widget>
            </child>
            <child>
              <widget class="GtkMenuItem" id="menuitem3">
                <property name="visible">True</property>
                <property name="label" translatable="yes">_View</property>
                <property name="use_underline">True</property>
              </widget>
            </child>
            <child>
              <widget class="GtkMenuItem" id="menuitem4">
                <property name="visible">True</property>
                <property name="label" translatable="yes">_Help</property>
                <property name="use_underline">True</property>
                <child>
                  <widget class="GtkMenu" id="menu3">
                    <property name="visible">True</property>
                    <child>
                      <widget class="GtkImageMenuItem" id="imagemenuitem10">
                        <property name="visible">True</property>
                        <property name="label" translatable="yes">gtk-about</property>
                        <property name="use_underline">True</property>
                        <property name="use_stock">True</property>
                      </widget>
                    </child>
                  </widget>
                </child>
              </widget>
            </child>
          </widget>
          <packing>
            <property name="expand">False</property>
          </packing>
        </child>
        <child>
          <widget class="GtkScrolledWindow" id="scrolledwindow1">
            <property name="visible">True</property>
            <property name="can_focus">True</property>
            <property name="hscrollbar_policy">GTK_POLICY_AUTOMATIC</property>
            <property name="vscrollbar_policy">GTK_POLICY_AUTOMATIC</property>
            <child>
              <widget class="GtkTextView" id="textview1">
                <property name="visible">True</property>
                <property name="can_focus">True</property>
              </widget>
            </child>
          </widget>
          <packing>
            <property name="position">1</property>
          </packing>
        </child>
        <child>
          <widget class="GtkStatusbar" id="statusbar1">
            <property name="visible">True</property>
            <property name="spacing">2</property>
          </widget>
          <packing>
            <property name="expand">False</property>
            <property name="position">2</property>
          </packing>
        </child>
      </widget>
    </child>
  </widget>
</glade-interface>
"""

if __name__ == '__main__':
    main()
