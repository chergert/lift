#!/usr/bin/env python

from setuptools import setup, find_packages
setup(
    name     = "lift",
    version  = "0.0.2",
    packages = find_packages(),
    scripts  = ['lift-generate.py'],

    author       = "Christian Hergert",
    author_email = "chris@dronelabs.com",
    description  = "Framework for rapidly building GNOME applications",
    license      = "LGPL-2",
    keywords     = "gtk gnome gconf glade framework",
    url          = "http://dronelabs.com/projects/lift",
)
