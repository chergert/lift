#!/usr/bin/env python
#
# Copyright (C) 2008 Christian Hergert <chris@dronelabs.com>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 
# 02110-1301 USA

class Service(object):
    """
    The Service class represents a, *gasp* a service to the application.
    It will live throughout the life of the application and can be
    retrieved through the Runtime singleton.

    If a Service had a name attribute of "data", it could be retreived
    from the Runtime single as such:

    >>> from lift import Runtime
    >>> Runtime.get_service('data')
    <DataService('data')>
    """
    name = None
    running = False

    def start_service(self):
        """Starts the service."""
        running = True

    def stop_service(self):
        """Stops the service."""
        running = False

class MultiService(Service):
    """
    This Service implementation allows for starting and stopping a
    collection of services together.

    See MultiService.add_service().
    """
    _services = None

    def __init__(self):
        self._services = []

    def add_service(self, service):
        if not self._services:
            raise AttributeError, "MultiService.__init__ was not called"
        self._services.append(service)

    def remove_service(self, service):
        if not self._services:
            raise AttributeError, "MultiService.__init__ was not called"

        if service in self._services:
            self._services.remove(service)

    def start_service(self):
        for service in self._services:
            service.start_service()
        Service.start_service(self)

    def stop_service(self):
        for service in self._services:
            service.stop_service()
        Service.stop_service(self)
