from cStringIO import StringIO
import sys
import time
import traceback

from zope.interface.advice import addClassAdvisor

_categories = {}

def logger(category):
    def setCategory(cls):
        cls._logger_category = category
        return cls
    addClassAdvisor(setCategory)

def debug(message, stream = sys.stdout):
    frame = sys._getframe(1)
    _self = frame.f_locals.get('self')
    if hasattr(_self, '_logger_category'):
        category = _self._logger_category
    else:
        category = ''

    print >> stream, '%s DEBUG: [%s] %s' % (
        time.strftime('%Y-%m-%d %H:%M:%S', time.localtime()),
        category, message)

def error(ex, stream = sys.stderr):
    frame = sys._getframe(1)
    _self = frame.f_locals.get('self')
    if hasattr(_self, '_logger_category'):
        category = _self._logger_category
    else:
        category = ''

    format = StringIO()
    traceback.print_exc(ex, file=format)
    format.seek(0)

    print >> stream, '%s ERROR: [%s] %s' % (
        time.strftime('%Y-%m-%d %H:%M:%S', time.localtime()),
        category, format.read()[:-1])

