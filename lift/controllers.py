#!/usr/bin/env python
#
# Copyright (C) 2008 Christian Hergert <chris@dronelabs.com>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 
# 02110-1301 USA

import os
from zope.interface.advice import addClassAdvisor
from lift import Runtime

class Controller(object):
    at_startup = False

    def __init__(self):
        if hasattr(self, '_lift_items'):
            for (callback, args, kwargs) in self._lift_items:
                callback(self, *args, **kwargs)

    def _attach(self, view):
        """Attaches a view to this controller."""
        if hasattr(self, 'view') and self.view:
            raise AttributeError, "A view is already attached"
        self.view = view
        view.attach_controller(self)
        self.attach(view)

    def _initialize(self):
        self.view.initialize()
        if hasattr(self, 'initialize'):
            self.initialize()

    def _detach(self):
        self.view.detach_controller(self)
        self.view = None
        self.detach()

    def attach(self, view):
        pass

    def detach(self):
        pass

def _view(self, *args, **kwargs):
    name, args = args[0], args[1:]
    ViewClass = Runtime._views[name]
    self._attach(ViewClass())

def view(name, *args, **kwargs):
    """
    This will attach a view to the controller.
    
    This should be used from within a class such as:
    
    >>> class MyController(Controller):
    ...     view('main')
    ...
    >>>
    
    @name: the name of the view such as 'main'
    """
    action = (_view, (name,) + args, kwargs)
    def installAction(cls):
        if not hasattr(cls, '_lift_items'):
            cls._lift_items = []
        cls._lift_items.append(action)
        return cls
    addClassAdvisor(installAction)
