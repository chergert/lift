#!/usr/bin/env python

import gtk
from gettext import gettext as _
from lift.widgets import console
from lift import Runtime

def showDebugConsole():
    w = gtk.Window(gtk.WINDOW_TOPLEVEL)
    w.set_default_size(500, 300)
    w.set_title(_('Debug Console'))
    w.set_border_width(12)
    w.show()

    v = gtk.VBox()
    v.set_spacing(12)
    w.add(v)
    v.show()

    c = console.PythonConsole({
        '__builtins__'  : __builtins__,
        'Runtime'       : Runtime,
    })
    c.connect('exited', lambda _: w.destroy())
    c.eval('print "You can access the runtime engine through \'Runtime\'"', False)
    v.pack_start(c, True, True, 0)
    c.show()

    bb = gtk.HButtonBox()
    bb.set_property('layout-style', gtk.BUTTONBOX_END)
    v.pack_end(bb, False, True, 0)
    bb.show()

    x = gtk.Button(stock = gtk.STOCK_CLOSE)
    x.connect('clicked', lambda _: w.destroy())
    bb.pack_start(x, False, True, 0)
    x.show()

    c.view.grab_focus()
    return w
