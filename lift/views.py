#!/usr/bin/env python
#
# Copyright (C) 2008 Christian Hergert <chris@dronelabs.com>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 
# 02110-1301 USA

import os
from zope.interface.advice import addClassAdvisor

from lift import Runtime

_layout_engines = {}

class View(object):
    """
    Base class for use by views within lift.  Provides hook actions during
    init, so if you override __init__, make sure it gets called.
    """
    controller = None

    def __init__(self):
        """
        Initializes the View and calls any requested callbacks during the
        class creation phase.  This is where the layout automagic happens.
        """
        if hasattr(self, '_lift_items'):
            for (callback, args, kwargs) in self._lift_items:
                callback(self, *args, **kwargs)
    
    def initialize(self):
        pass
    
    def attach_controller(self, controller):
        self.controller = controller

    def detach_controller(self):
        self.controller = None
    
def layout(engine, *args, **kwargs):
    """
    This will attach a layout to the view allowing for auto loading of
    widgets.
    
    This should be used from within a class such as:
    
    >>> class MyView(View):
    ...     layout('glade', filename = 'myview.glade')
    ...
    >>>
    
    @engine: the layout engine to use such as 'glade'
    """
    if engine not in _layout_engines:
        raise AttributeError, 'Engine %s could not be found' % engine
    
    action = (_layout_engines[engine], args, kwargs)
    def installAction(cls):
        if not hasattr(cls, '_lift_items'):
            cls._lift_items = []
        cls._lift_items.append(action)
        return cls
    addClassAdvisor(installAction)

def layout_engine(name):
    """
    Decorator to install a new layout engine for use within the layout()
    view helper.
    
    Example:
    
    @layout_engine
    def glade_layout_engine(instance, *args, **kwargs):
        pass
    """
    def register_layout(func):
        _layout_engines[name] = func
    return register_layout

def get_default_filename(self, suffix = ''):
    """
    Returns what the default template name would be for a particular
    class instance.
    """
    module = self.__class__.__module__.lower()
    if module == '__main__':
        module = ''
    else:
        # trim off the app module
        module = '.'.join(module.split('.')[1:])
    klass = self.__class__.__name__.lower()
    config = Runtime.get_config()
    if hasattr(config, 'VIEW_PATH'):
        viewpath = config.VIEW_PATH
    else:
        viewpath = 'data'
    return os.path.join(viewpath,
                        module.replace('.', os.path.sep),
                        klass) + suffix

@layout_engine('glade')
def glade_layout_engine(self, *args, **kwargs):
    """
    Layout engine using glade for creating the widgets.
    """
    import gtk.glade
    
    filename = kwargs.get('filename', get_default_filename(self, '.glade'))
    gxml = gtk.glade.XML(filename)
    connect_signals = kwargs.get('connect_signals', True)
    connect_widgets = kwargs.get('connect_widgets', True)
    
    # connect view signals if requested
    if connect_signals:
        gxml.signal_autoconnect(self)
        
    # attach widgets if requested
    if connect_widgets:
        widgets = gxml.get_widget_prefix('')
        for widget in widgets:
            name = gtk.glade.get_widget_name(widget)
            if name and not hasattr(self, name):
                setattr(self, name, widget)
    
    self.layout = gxml

@layout_engine('gtk')
def gtk_layout_engine(self, *args, **kwargs):
    """
    Layout engine using GtkBuilder for creating the widgets.
    """
    import gtk
    
    builder = gtk.Builder()
    filename = kwargs.get('filename', get_default_filename(self, '.gtkui'))
    builder.add_from_file(filename)
    connect = kwargs.get('connect_signals', True)
    
    # connect view signals if requested
    if connect:
        builder.connect_signals(self)
        
    self.layout = builder

if __name__ == '__main__':
    class MyView(View):
        layout('glade')
    import gtk
    v = MyView()
    v.adroit_window.connect('destroy', gtk.main_quit)
    v.adroit_window.show()
    gtk.main()
