#!/usr/bin/env python
#
# Copyright (C) 2008 Christian Hergert <chris@dronelabs.com>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 
# 02110-1301 USA

import gettext
import os
import sys

class Runtime(object):
    """
    Runtime provides centralized access throughout the applicatoin.
    It is a singleton object instantiated during import of the
    lift module.
    """
    def start(self):
        """Starts the lift application."""
        for service in self._services:
            try:
                service.start_service()
            except Exception, ex:
                print ex

        # Find controllers/views that are visible at startup
        for controller in self._controllers:
            if hasattr(controller, 'auto') and controller.auto:
                try:
                    inst = controller()
                    inst._initialize()
                except Exception, ex:
                    print ex

    def stop(self):
        """Stops the lift application."""
        for service in self._services:
            try:
                service.stop_service()
            except Exception, ex:
                print ex

    def initialize(self, package):
        """Initializes services for the application."""
        self._package = package
        self._setupServices(package)
        self._setupModels(package)
        self._setupSettings(package)
        self._setupCommands(package)
        self._setupControllers(package)
        self._setupViews(package)

    def get_config(self):
        """Returns the config module for the application."""
        return tryImport(self._package.__name__ + '.config')

    def get_setting(self, name):
        return self._settings[name]

    def get_user_path(self, *path):
        config = self.get_config()
        if hasattr(config, 'APP_NAME'):
            app = config.APP_NAME
        else:
            app = sys.argv[0]

        basePath = os.path.join(os.path.expanduser('~'), '.config', app)

        if not os.path.exists(basePath):
            os.makedirs(basePath)

        return os.path.join(basePath, *path)

    def get_service_named(self, name):
        return self._services_named[name]

    def _setupServices(self, package):
        """Discovers and instantiates services."""
        from lift.services import Service
        s = tryImport(package.__name__ + '.services')
        services = s and getPlugins(s, Service) or []
        self._services = [apply(s) for s in services]

        self._services_named = {}
        for service in self._services:
            if hasattr(service, 'name'):
                self._services_named[service.name] = service

    def _setupModels(self, package):
        """Discovers and instantiates models."""

    def _setupSettings(self, package):
        """Discovers and instantiates settings."""
        from lift.settings import Settings
        s = tryImport(package.__name__ + '.settings')
        settings = s and getPlugins(s, Settings) or []
        self._settings = {}
        for setting in settings:
            self._settings[setting.settings_name] = setting()

    def _setupCommands(self, package):
        """Discovers and instantiates commands."""
        from lift.commands import Command
        c = tryImport(package.__name__ + '.commands')
        self._commands = c and getPlugins(c, Command) or []

    def _setupControllers(self, package):
        """Discovers and instantiates controllers."""
        from lift.controllers import Controller
        c = tryImport(package.__name__ + '.controllers')
        self._controllers = c and getPlugins(c, Controller) or []

    def _setupViews(self, package):
        """Discovers and instantiates views."""
        from lift.views import View
        v = tryImport(package.__name__ + '.views')
        views = v and getPlugins(v, View) or []
        self._views = dict([(v.name, v) for v in views])

Runtime = Runtime()

def getPlugins(package, base):
    """
    Extracts all of the classes deriving from @base.
    """
    name = package.__name__
    path = package.__file__
    path = path[:path.rindex('/')]

    # TODO: Make work with zip'd packages and such
    modules = (a[:-3] for a in os.listdir(path) if a.endswith('.py'))
    plugins = []

    for module in modules:
        module = tryImport(name + '.' + module, True)
        if module:
            for i in dir(module):
                item = getattr(module, i)
                if (isinstance(item, type)
                    and issubclass(item, base)
                    and item != base):
                    plugins.append(item)

    return plugins

def _setupGettext(domain, localedir = ''):
    gettext.bindtextdomain(domain, localedir)
    gettext.textdomain(domain)

def _verifyDeps():
    """Checks for missing dependencies in lift."""
    requiredModules = ['gtk', 'gconf', 'gettext']
    missingModules = []

    for moduleName in requiredModules:
        if not tryImport(moduleName):
            missingModules.append(moduleName)

    if len(missingModules) == 0:
        return True

    print 'You are missing the following modules required to run'
    print 'the lift application.'
    print ''

    for moduleName in missingModules:
        print '  *', moduleName

    print ''

def createInstance(typeName):
    """ 
    Creates an instance of the type found at @typeName.
    
    @typeName: the package.Name of the class.
    
    Returns: an instance of the class
    
    >>> createInstance('marina.sources.vfs.VfsSource')
    >>> <marina.sources.vfs.VfsSource at 0x8688c2c>
    """
    typeSepPos = typeName.rindex('.')
    
    packageName = typeName[:typeSepPos]
    className = typeName[typeSepPos + 1:]
    
    module = __import__(packageName, fromlist=packageName)
    clsObj = getattr(module, className)

    return clsObj()

def tryImport(moduleName, printErrors = False):
    """
    Try to import module @moduleName.

    @moduleName: a str containing the module to import
    """
    try:
        return __import__(moduleName, None, None, moduleName)
    except ImportError, ex:
        if printErrors:
            print 'Could not import %s: %s' % (moduleName, str(ex))
        return None

def main(package, args = sys.argv[1:]):
    """
    This convienence method will start a lift application using
    the contents within the @package.  To test your lift application
    from within the root of your project (assuming package is available
    from the root of your project) run the following:

    >>> import lift
    >>> lift.main('myapp')
    """
    # Verify we have our runtime dependencies
    _verifyDeps() or sys.exit(1)

    try:
        py_package = __import__(package)
    except ImportError:
        print 'Could not load package', package
        return

    # initialize i18n and l10n
    packageConfig = tryImport(package + '.config')
    if packageConfig:
        if hasattr(packageConfig, 'GETTEXT_PACKAGE'):
            domain = packageConfig.GETTEXT_PACKAGE
        else:
            domain = ''
        if hasattr(packageConfig, "PACKAGE_LOCALEDIR"):
            localedir = packageConfig.PACKAGE_LOCALEDIR
        else:
            localedir = ''
        _setupGettext(domain, localedir)
    else:
        print 'could not load config'

    # reflect components
    Runtime.initialize(py_package)

    # Run the debug console
    if '--debug' in args:
        from lift import helpers
        helpers.showDebugConsole()

    # startup components
    Runtime.start()

    import gtk

    try:
        gtk.main()
    except KeyboardInterrupt:
        pass

    # stop the components
    Runtime.stop()
