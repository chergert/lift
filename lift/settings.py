#!/usr/bin/env python
#
# Copyright (C) 2008 Christian Hergert <chris@dronelabs.com>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 
# 02110-1301 USA

import pygtk; pygtk.require('2.0')
import gconf

from lift import Runtime

class Setting(object):
    """
    Base class for setting type implementations.  This manages the
    callback infrastucture and such.
    """
    def __init__(self, name):
        self._name = name
        self._callbacks = []
        self._notify = None

    def set_client(self, client, root):
        self._client = client
        path = "%s/%s" % (root, self._name)
        self._path = path.replace("//", "/")

    def get_value(self):
        return self._client.get(self._path)

    def set_value(self, value):
        pass

    def changed(self, callback, *args, **kwargs):
        """
        Sets a callback to be executed when the value is changed
        in gconf storage.
        """
        if not self._notify:
            self._notify = self._client.notify_add(self._path, self._changed)
        self._callbacks.append((callback, args, kwargs))

    def _changed(self, client, conn_id, entry, _):
        for (callback, args, kwargs) in self._callbacks:
            try:
                callback(self, entry.get_value(), *args, **kwargs)
            except Exception, ex:
                print ex

    def __int__(self):
        return int(self.get_value())

    def __str__(self):
        return str(self.get_value())

class IntSetting(Setting):
    def set_value(self, value):
        self._client.set_int(self._path, value)

class StrSetting(Setting):
    def set_value(self, value):
        self._client.set_string(self._path, value)

class Settings(object):
    """
    Settings metaclass provides a system for managing settings to
    gconf backend.  It allows monitoring of those values for callbacks
    within your application.
    """
    def __init__(self, name = '', path = ''):
        """
        Creates a new Settings instance which binds properties to
        gconf using GCONF_ROOT/path/prop_name.
        """
        self._name = name
        self._path = path

        if not name:
            self._name = self.settings_name

        if not path:
            self._path = self.settings_path

        config = Runtime.get_config()
        root = config.SETTINGS_ROOT + self._path

        # our gconf client
        self._client = gconf.client_get_default()
        rootdir = root.endswith('/') and root[:-1] or root
        self._client.add_dir(rootdir, gconf.CLIENT_PRELOAD_NONE)

        # lambda to retreive our attributes which are settings
        issetting = lambda a: isinstance(getattr(self, a), Setting)
        attrs = (a for a in dir(self) if issetting(a))

        for attr in list(attrs):
            attr = getattr(self, attr)
            attr.set_client(self._client, root)

    def __setattr__(self, name, value):
        if hasattr(self, name):
            attr = getattr(self, name)
            if isinstance(attr, Setting):
                attr.set_value(value)
                return
        object.__setattr__(self, name, value)

if __name__ == '__main__':
    class LiftSettings(Settings):
        x_pos = IntSetting('x_pos')
        y_pos = IntSetting('y_pos')
        width = IntSetting('width')
        height = IntSetting('height')
    s = LiftSettings()
    import gtk
    def puts(*a):
        print a
        gtk.main_quit()
    s.height.changed(lambda *a: puts(a))
    s.x_pos = 5
    s.height = 500
    s.height = 300
    gtk.main()
