#!/usr/bin/env python
#
# Copyright (C) 2008 Christian Hergert <chris@dronelabs.com>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 
# 02110-1301 USA

class Command(object):
    """
    The command class provides an abstraction on an operation that
    may be undo-able.  Use commands to encapsulate your logic so
    that you may provide rich undo/redo support to your application.
    """
    # The text used on the undo/redo labels ('add project')
    text = ''

    # True if the command is undo-able.
    can_undo = False

    # True if the command can be redone.
    # redo can only happen after an undo.
    can_redo = False

    def __init__(self, text = '', can_undo = False, can_redo = False,
                 do = None, undo = None, args = []):
        """
        Optional init for a Command.  This allows the use of a
        base command without the need to inherit.

        @text: the undo/redo text
        @can_undo: if the command can undo
        @can_redo: if the command can be redone
        @do: a function to perform for do
        @undo: a function to perform for undo
        @args: args to pass to function after the command
        """
        self.text = text
        self.can_undo = can_undo
        self.can_redo = can_redo
        self._do = do
        self._undo = undo
        self._args = args

    def do(self):
        """Performs the commands operation."""
        if hasattr(self, '_do'):
            self._do(self, *self._args)

    def undo(self):
        """Performs the undo of the commands operation."""
        if hasattr(self, '_undo'):
            self._undo(self, *self._args)

    def chain(self, other):
        """
        Attempts to chain another command onto this command.  This
        might be done if lots of little edits are done to an object
        and it makes sense to undo/redo them as a whole.

        Returns True if the command was chained.
        """
        return False

class CommandManager(object):
    """
    The CommandManager class provides an undo/redo stack for applications
    that use commands to perform operations.
    """
    _undo_stack = None
    _redo_stack = None

    # True if there is an operation that can be undone
    can_undo = False

    # True if there is an operation that cna be redone
    can_redo = False

    # The undo text for the next undo-able command
    undo_text = ''

    # The redo text for the next redo-able command
    redo_text = ''

    def __init__(self):
        self._undo_stack = []
        self._redo_stack = []
        self._changed = []

    def changed(self, callback, *args):
        self._changed.append((callback, args))

    def do(self, command):
        """Performs a command and pushes it onto the undo stack."""
        # First check if the command can be chained
        if len(self._undo_stack):
            if self._undo_stack[0].chain(command):
                return True

        # Clear any redo-able commands, they are no longer valid
        self._redo_stack = []

        # Perform the command since it could not be chained
        command.do()

        # If the command is undoable, add to stack and update fields
        if command.can_undo:
            self._undo_stack.insert(0, command)
            self._update()

        return True

    def undo(self):
        """Attempts to undo the next undo-able task."""
        if not self.can_undo:
            return

        # get the command and update our stack
        command, self._undo_stack = self._undo_stack[0], self._undo_stack[1:]

        # really, this should always be True
        if command.can_undo:
            command.undo()

        # push onto the redo stack if we can
        if command.can_redo:
            self._redo_stack.insert(0, command)

        self._update()

        return True

    def redo(self):
        """Attempts to redo the next redo-able task."""
        if not self.can_redo:
            return

        # get the command and update our stack
        command, self._redo_stack = self._redo_stack[0], self._redo_stack[1:]

        # really, this should always be True
        if command.can_redo:
            command.do()

        # really, this should be true too.
        # push onto the undo stack if we can
        if command.can_undo:
            self._undo_stack.insert(0, command)

        self._update()

        return True

    def _update(self):
        self.can_undo = len(self._undo_stack) > 0
        self.can_redo = len(self._redo_stack) > 0

        if self.can_undo:
            self.undo_text = self._undo_stack[0].text
        else:
            self.undo_text = ''
        
        if self.can_redo:
            self.redo_text = self._redo_stack[0].text
        else:
            self.redo_text = ''

        for callback,args in self._changed:
            try:
                callback(*args)
            except Exception, ex:
                print ex
